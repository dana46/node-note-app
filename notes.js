const fs = require('fs');

/* Fetch all notes*/
var fetchNotes = () => { 
  try{
    var notesString = fs.readFileSync('notes-data.json');
    return JSON.parse(notesString);
  }catch (e) {
    return [];
  }
};

/* Save a note */
var saveNote = (notes) => {
  fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

/* Adding a note */
var addNote = (title, body) => {
  var notes = fetchNotes();
  var note = {
    title,
    body
  };

  var duplicateNotes = notes.filter((note) => note.title === title);
  if(duplicateNotes.length === 0){
    notes.push(note);
    saveNote(notes);
    return note;
  }

};

/* wrapper */
var getAll = () => {
  return  fetchNotes();
};

/* Find a note */
var getNote = (title) => {
  var notes = fetchNotes();
  var searchedNote = notes.filter((note) => note.title === title);
  return searchedNote[0];
};

/* Delete a note */
var removeNote = (title) => {
  var notes = fetchNotes();
  var filteredNotes = notes.filter((note) => note.title !== title);
  saveNote(filteredNotes);
  return notes.length !== filteredNotes.length;
};

/* Printing a note */
var logNote = (note) => {
  console.log('^_^');
  console.log(`Title : ${note.title}`);
  console.log(`Body : ${note.body}`);
};

module.exports = {
  addNote,
  getAll,
  getNote,
  removeNote,
  logNote
};

